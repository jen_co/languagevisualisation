import controlP5.*;

String[] data;
ArrayList<Word> words = new ArrayList<Word>();
ArrayList<Word> trWords = new ArrayList<Word>();
Alphabet alph;

int blockSize = 10;
int canvasWidth;// = 1200;
int canvasHeight;// = 700;
int offsetY = 0;

ControlP5 cp5;
DropdownList d1;

Dictionary dict;
Translation tr = new Translation();
String lang = tr.langsFullName[0];

void setup()
{
  size(displayWidth, displayHeight);
  canvasWidth = width;
  canvasHeight = height;
  background(0);
  
  //UI
  cp5 = new ControlP5(this);
  d1 = cp5.addDropdownList("languages").setPosition(20, 20);
  customizeDropDown(d1); 
  PFont pfont = createFont("Arial",20,true); 
  ControlFont font = new ControlFont(pfont,10);
  cp5.setFont(font);
 
  alph = new Alphabet();
  dict = new Dictionary("web2");
  drawWords();
}

void drawWords()
{
  background(0);
  
  //selecting a large pool size of words so that if translation api fails to find a translation then look for more words that do. Hoping that in pool of 2000 there will be roughly 100 words that have a valid translation
  String[] dictWords = dict.getRandomWords(600);
  
  words.clear();
  trWords.clear();
 
  tr.translate(dictWords, lang);
  
  for (int j = 0; j < tr.baseWords.length; j++)
  {
    Word word = new Word(tr.baseWords[j], alph);
    Word trWord =  new Word(tr.translatedWords[j], alph);
    
    word.drawColourBlock(blockSize, j, canvasWidth/2, offsetY, true);
    trWord.drawColourBlock(blockSize, j, canvasWidth/2, offsetY, false);
    
    words.add(word);
    trWords.add(trWord);
  } 
}

void draw()
{ 
  fill(0);
  rect(canvasWidth - 210, 0, 300,canvasHeight);
  rect(0, 0, 300,canvasHeight);
  
  for (int i = 0; i < words.size(); i++)
  {
    if (mouseY <= words.get(i).y + blockSize && mouseY >= words.get(i).y && mouseX > d1.getWidth() + 200)
    {
      words.get(i).printText(150);
      trWords.get(i).printText(canvasWidth - 200);
    } 
  }
}

void customizeDropDown(DropdownList ddl) 
{
  ddl.setBackgroundColor(#218C8D);
  ddl.setItemHeight(20);
  ddl.setBarHeight(15);
  ddl.captionLabel().set(lang);
  for (int i = 0; i < tr.langsFullName.length; i++)
  {
    ddl.addItem(tr.langsFullName[i], i);
  }
  ddl.setColorBackground(color(60));
  ddl.setColorForeground(#218C8D);
  ddl.setColorActive(#218C8D);
}

void controlEvent(ControlEvent theEvent) 
{
  if (theEvent.isGroup()) 
  { 
    lang = (d1.getItem((int)theEvent.getGroup().getValue())).getText();
    drawWords();
  } 
}
