# Project Name

Language Synasthesia/Visualisation

#Description

This app takes a random sample of English words from the list of dictionary words. It then translates that random sample to a chosen language using Yandex API (https://tech.yandex.com). It then maps every letter of the alphabet to a specific colour and presents each of the words from both languages as blocks of colour side by side. My intention here, is to give a more abstract (and pretty) overview of the differences between two languages as well as a new way of visualising a language. 

At the moment, the default base language is English and the translation options include French, German, Italian, Dutch, but I have written the code so that more latin alphabet languages can easily be added. In addition with a little bit of extra config, non-latin aphabet languages can also be added. 

When a language is selected from the dropdown menu and random number of words is selected from the word list, translated, and turned into its colour representation. When hovering over the colour representation, the actual word and its translation is shown on either side of the screen.

This app was written using Processing. I find this environment a really nice tool for quick protoyping of app ideas and it especially useful for graphical applications.


## Usage

To run it on Windows 8, unzip "application.windows32" and run. The java runtime needs to be installed and unfortunately the SmartScreen warning needs to be bypassed (when the warning comes up, click "More Info" and then select "Run Anyway") (When exporting a Processing sketch as an application, it doesn't seem to allow the application to be signed or registered and so this becomes a problem for Windows SmartScreen).

To run it OSX, unzip "application.macosx" and run. As with Windows SmartScreen, unfortunately to run this app it may be neccesary to go to System Preferences > Security & Privacy > General > Allow applications downloaded from Anywhere. 

If, Processing is installed on your machine, you could also run the LangSynasthesia.pde in present mode.

