import java.text.Normalizer;
import java.util.regex.Pattern;

public class Translation
{ 
  //ideally would have a dictionary/lookup table for this
  String[] langsFullName = {"French", "German", "Italian", "Dutch"};
  String[] langsKey = {"fr", "de", "it", "nl"};
  
  String[] baseWords;
  String[] translatedWords;
  
  public void translate(String[] wordsPool, String lang)
  {
    String wordsJoined = join(wordsPool, "&text=");
    String langKey = getLangKey(lang);
    
    if (langKey != null)
    {
      JSONObject json = loadJSONObject("https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20150408T200327Z.aa5febc4f3af8e8e.d1301256d08b7013a7bd18b9b074ac442b9178f9&lang=en-" + langKey + "&text=" + wordsJoined);
      JSONArray translated = json.getJSONArray("text");
      
      getValidTranslations(wordsPool, translated);
    }
  }
  
  //this method tries to account for the the failure of the translation api to succesfully translate a lot of the words in the dictionary list
  private void getValidTranslations(String[] words, JSONArray trWords)
  {
     ArrayList<String> trWordsValid = new ArrayList<String>();
     ArrayList<String> wordsValid = new ArrayList<String>();
     
     for (int i = 0; i < trWords.size(); i++)
     {
       String trWordDeaccent = deAccent(trWords.getString(i));
       
       //when api fails to translate it just returns the original word. So we discard these
       if (!words[i].equals(trWordDeaccent))
       {
         wordsValid.add(words[i]);
         trWordsValid.add(trWordDeaccent);
       }     
     }
     
     translatedWords = new String[trWordsValid.size()];
     translatedWords = trWordsValid.toArray(translatedWords);
     
     baseWords = new String[wordsValid.size()];
     baseWords = wordsValid.toArray(baseWords);   
     
  }
  
  private String deAccent(String str) 
  {
    String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD); 
    Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
    return pattern.matcher(nfdNormalizedString).replaceAll("");
  }
  
  private String getLangKey(String lang)
  {
    for (int i = 0; i < langsFullName.length; i++)
    {
      if (langsFullName[i] == lang)
        return langsKey[i];
    }
    return null;
  }
  
}
