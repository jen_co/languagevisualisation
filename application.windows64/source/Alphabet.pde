public class Alphabet
{
  Letter[] alphLetters;
  
  color[] cols = 
  {#2F343A, #717D8C, #DDDDDD, #BDB69C, #80CEB9, #41AAC4, 
   #218C8D, #6CCECB, #F9E559, #EF7126, #8EDC9D, #787A40, 
   #9FBF8C, #C8AB65, #F69A98, #D4CBC3, #F2C249, #E6772E, 
   #4DB3B3, #E64A45, #217C7E, #F3EFE0, #3399FF, #9A3334,
   #FFD800, #587058, #E86850};

  char[] alphChars = 
  {'a', 'b', 'c', 'd', 'e', 'f',
   'g', 'h', 'i', 'j', 'i', 'k', 
   'l', 'm', 'n', 'o', 'p', 'q', 
   'r', 's', 't', 'u', 'v', 'w', 
   'x', 'y', 'z'};  
  
  public Alphabet()
  { 
    alphLetters = new Letter[alphChars.length];  
    for (int i = 0; i < alphChars.length; i++)
    {
      alphLetters[i] = new Letter(alphChars[i], cols[i]);
    }  
  }
  
  //can have the option to override default latin alphabet
  public Alphabet(char[] alphChars, color[] cols)
  {
    this.alphChars = alphChars;
    this.cols = cols;
    
    alphLetters = new Letter[alphChars.length];  
    for (int i = 0; i < alphChars.length; i++)
    {
      alphLetters[i] = new Letter(alphChars[i], cols[i]);
    }  
  }
  
  
  
  public Letter getLetter(char character)
  {
    
    for (int i = 0; i < alphLetters.length; i++)
    {
        if (Character.toLowerCase(alphLetters[i].character) == Character.toLowerCase(character))
        {
          return alphLetters[i];
        }
    }
    
    return null;
  }
}
