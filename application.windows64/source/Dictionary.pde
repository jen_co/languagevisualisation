public class Dictionary
{
  
  String[] words;
  
  public Dictionary(String source)
  {
    words = loadStrings(source); 
  }
 
  public String[] getRandomWords(int numWords)
  {
    String[] randWords = new String[numWords];
    for (int i = 0; i < numWords; i++)
    {
      boolean foundDecentWord = false; 
      while (!foundDecentWord)
      {
        int randomIndex = (int)random(words.length);
        String wordString = words[randomIndex];
        if (!Character.isUpperCase(wordString.charAt(0)))
        {
          randWords[i] = wordString;
          foundDecentWord = true;
        }
      }
    }
    return randWords;
  } 
}
