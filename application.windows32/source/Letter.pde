public class Letter
{
  char character;
  color col;
  
  public Letter(char character, color col)
  {
    this.character = character;
    this.col = col;
  }
}
