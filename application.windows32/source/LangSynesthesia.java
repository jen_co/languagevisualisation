import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import controlP5.*; 
import java.text.Normalizer; 
import java.util.regex.Pattern; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class LangSynesthesia extends PApplet {



String[] data;
ArrayList<Word> words = new ArrayList<Word>();
ArrayList<Word> trWords = new ArrayList<Word>();
Alphabet alph;

int blockSize = 10;
int canvasWidth;// = 1200;
int canvasHeight;// = 700;
int offsetY = 0;

ControlP5 cp5;
DropdownList d1;

Dictionary dict;
Translation tr = new Translation();
String lang = tr.langsFullName[0];

public void setup()
{
  size(displayWidth, displayHeight);
  canvasWidth = width;
  canvasHeight = height;
  background(0);
  
  //UI
  cp5 = new ControlP5(this);
  d1 = cp5.addDropdownList("languages").setPosition(20, 20);
  customizeDropDown(d1); 
  PFont pfont = createFont("Arial",20,true); 
  ControlFont font = new ControlFont(pfont,10);
  cp5.setFont(font);
 
  alph = new Alphabet();
  dict = new Dictionary("web2");
  drawWords();
}

public void drawWords()
{
  background(0);
  
  //selecting a large pool size of words so that if translation api fails to find a translation then look for more words that do. Hoping that in pool of 2000 there will be roughly 100 words that have a valid translation
  String[] dictWords = dict.getRandomWords(600);
  
  words.clear();
  trWords.clear();
 
  tr.translate(dictWords, lang);
  
  for (int j = 0; j < tr.baseWords.length; j++)
  {
    Word word = new Word(tr.baseWords[j], alph);
    Word trWord =  new Word(tr.translatedWords[j], alph);
    
    word.drawColourBlock(blockSize, j, canvasWidth/2, offsetY, true);
    trWord.drawColourBlock(blockSize, j, canvasWidth/2, offsetY, false);
    
    words.add(word);
    trWords.add(trWord);
  } 
}

public void draw()
{ 
  fill(0);
  rect(canvasWidth - 210, 0, 300,canvasHeight);
  rect(0, 0, 300,canvasHeight);
  
  for (int i = 0; i < words.size(); i++)
  {
    if (mouseY <= words.get(i).y + blockSize && mouseY >= words.get(i).y && mouseX > d1.getWidth() + 200)
    {
      words.get(i).printText(150);
      trWords.get(i).printText(canvasWidth - 200);
    } 
  }
}

public void customizeDropDown(DropdownList ddl) 
{
  ddl.setBackgroundColor(0xff218C8D);
  ddl.setItemHeight(20);
  ddl.setBarHeight(15);
  ddl.captionLabel().set(lang);
  for (int i = 0; i < tr.langsFullName.length; i++)
  {
    ddl.addItem(tr.langsFullName[i], i);
  }
  ddl.setColorBackground(color(60));
  ddl.setColorForeground(0xff218C8D);
  ddl.setColorActive(0xff218C8D);
}

public void controlEvent(ControlEvent theEvent) 
{
  if (theEvent.isGroup()) 
  { 
    lang = (d1.getItem((int)theEvent.getGroup().getValue())).getText();
    drawWords();
  } 
}
public class Alphabet
{
  Letter[] alphLetters;
  
  int[] cols = 
  {0xff2F343A, 0xff717D8C, 0xffDDDDDD, 0xffBDB69C, 0xff80CEB9, 0xff41AAC4, 
   0xff218C8D, 0xff6CCECB, 0xffF9E559, 0xffEF7126, 0xff8EDC9D, 0xff787A40, 
   0xff9FBF8C, 0xffC8AB65, 0xffF69A98, 0xffD4CBC3, 0xffF2C249, 0xffE6772E, 
   0xff4DB3B3, 0xffE64A45, 0xff217C7E, 0xffF3EFE0, 0xff3399FF, 0xff9A3334,
   0xffFFD800, 0xff587058, 0xffE86850};

  char[] alphChars = 
  {'a', 'b', 'c', 'd', 'e', 'f',
   'g', 'h', 'i', 'j', 'i', 'k', 
   'l', 'm', 'n', 'o', 'p', 'q', 
   'r', 's', 't', 'u', 'v', 'w', 
   'x', 'y', 'z'};  
  
  public Alphabet()
  { 
    alphLetters = new Letter[alphChars.length];  
    for (int i = 0; i < alphChars.length; i++)
    {
      alphLetters[i] = new Letter(alphChars[i], cols[i]);
    }  
  }
  
  //can have the option to override default latin alphabet
  public Alphabet(char[] alphChars, int[] cols)
  {
    this.alphChars = alphChars;
    this.cols = cols;
    
    alphLetters = new Letter[alphChars.length];  
    for (int i = 0; i < alphChars.length; i++)
    {
      alphLetters[i] = new Letter(alphChars[i], cols[i]);
    }  
  }
  
  
  
  public Letter getLetter(char character)
  {
    
    for (int i = 0; i < alphLetters.length; i++)
    {
        if (Character.toLowerCase(alphLetters[i].character) == Character.toLowerCase(character))
        {
          return alphLetters[i];
        }
    }
    
    return null;
  }
}
public class Dictionary
{
  
  String[] words;
  
  public Dictionary(String source)
  {
    words = loadStrings(source); 
  }
 
  public String[] getRandomWords(int numWords)
  {
    String[] randWords = new String[numWords];
    for (int i = 0; i < numWords; i++)
    {
      boolean foundDecentWord = false; 
      while (!foundDecentWord)
      {
        int randomIndex = (int)random(words.length);
        String wordString = words[randomIndex];
        if (!Character.isUpperCase(wordString.charAt(0)))
        {
          randWords[i] = wordString;
          foundDecentWord = true;
        }
      }
    }
    return randWords;
  } 
}
public class Letter
{
  char character;
  int col;
  
  public Letter(char character, int col)
  {
    this.character = character;
    this.col = col;
  }
}



public class Translation
{ 
  //ideally would have a dictionary/lookup table for this
  String[] langsFullName = {"French", "German", "Italian", "Dutch"};
  String[] langsKey = {"fr", "de", "it", "nl"};
  
  String[] baseWords;
  String[] translatedWords;
  
  public void translate(String[] wordsPool, String lang)
  {
    String wordsJoined = join(wordsPool, "&text=");
    String langKey = getLangKey(lang);
    
    if (langKey != null)
    {
      JSONObject json = loadJSONObject("https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20150408T200327Z.aa5febc4f3af8e8e.d1301256d08b7013a7bd18b9b074ac442b9178f9&lang=en-" + langKey + "&text=" + wordsJoined);
      JSONArray translated = json.getJSONArray("text");
      
      getValidTranslations(wordsPool, translated);
    }
  }
  
  //this method tries to account for the the failure of the translation api to succesfully translate a lot of the words in the dictionary list
  private void getValidTranslations(String[] words, JSONArray trWords)
  {
     ArrayList<String> trWordsValid = new ArrayList<String>();
     ArrayList<String> wordsValid = new ArrayList<String>();
     
     for (int i = 0; i < trWords.size(); i++)
     {
       String trWordDeaccent = deAccent(trWords.getString(i));
       
       //when api fails to translate it just returns the original word. So we discard these
       if (!words[i].equals(trWordDeaccent))
       {
         wordsValid.add(words[i]);
         trWordsValid.add(trWordDeaccent);
       }     
     }
     
     translatedWords = new String[trWordsValid.size()];
     translatedWords = trWordsValid.toArray(translatedWords);
     
     baseWords = new String[wordsValid.size()];
     baseWords = wordsValid.toArray(baseWords);   
     
  }
  
  private String deAccent(String str) 
  {
    String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD); 
    Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
    return pattern.matcher(nfdNormalizedString).replaceAll("");
  }
  
  private String getLangKey(String lang)
  {
    for (int i = 0; i < langsFullName.length; i++)
    {
      if (langsFullName[i] == lang)
        return langsKey[i];
    }
    return null;
  }
  
}
public class Word
{
  String word;
  Alphabet alph;
  int x;
  int y;
  
  public Word(String word, Alphabet alph)
  {
    this.word = word;  
    this.alph = alph;
  }
  
  public Letter[] getLetters()
  {
    char[] charArr = word.toCharArray();
    Letter[] letters = new Letter[charArr.length];
    for (int i = 0; i < charArr.length; i++)
    {
      letters[i] = alph.getLetter(charArr[i]);
    }
    
    return letters;
  }
  
  public void drawColourBlock(int blockSize, int posY, int midPointX, int offsetY, boolean alignRight)
  {
    Letter[] letters = getLetters();
    
    for (int k = 0; k < letters.length; k++)
    {
      //maybe an erroneous character or for example a -
      if(letters[k]!= null)
      {
        fill(letters[k].col);
 
        if (alignRight)
          rect(k*blockSize + midPointX-letters.length*blockSize, blockSize*posY + offsetY, blockSize, blockSize);
        else
          rect(k*blockSize + midPointX + 20, blockSize*posY + offsetY, blockSize, blockSize);
      }
    }
    
    setLocation(0, blockSize*posY + offsetY);        
  }
  
  public void printText(int posX)
  {
    fill(255);
    textSize(14);
    text(word, posX, y  + blockSize); 
  }
  
  public void setLocation(int x, int y)
  {
    this.x = x;
    this.y = y;
  }
  
  
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--full-screen", "--bgcolor=#666666", "--stop-color=#cccccc", "LangSynesthesia" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
