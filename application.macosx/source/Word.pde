public class Word
{
  String word;
  Alphabet alph;
  int x;
  int y;
  
  public Word(String word, Alphabet alph)
  {
    this.word = word;  
    this.alph = alph;
  }
  
  public Letter[] getLetters()
  {
    char[] charArr = word.toCharArray();
    Letter[] letters = new Letter[charArr.length];
    for (int i = 0; i < charArr.length; i++)
    {
      letters[i] = alph.getLetter(charArr[i]);
    }
    
    return letters;
  }
  
  public void drawColourBlock(int blockSize, int posY, int midPointX, int offsetY, boolean alignRight)
  {
    Letter[] letters = getLetters();
    
    for (int k = 0; k < letters.length; k++)
    {
      //maybe an erroneous character or for example a -
      if(letters[k]!= null)
      {
        fill(letters[k].col);
 
        if (alignRight)
          rect(k*blockSize + midPointX-letters.length*blockSize, blockSize*posY + offsetY, blockSize, blockSize);
        else
          rect(k*blockSize + midPointX + 20, blockSize*posY + offsetY, blockSize, blockSize);
      }
    }
    
    setLocation(0, blockSize*posY + offsetY);        
  }
  
  public void printText(int posX)
  {
    fill(255);
    textSize(14);
    text(word, posX, y  + blockSize); 
  }
  
  public void setLocation(int x, int y)
  {
    this.x = x;
    this.y = y;
  }
  
  
}
